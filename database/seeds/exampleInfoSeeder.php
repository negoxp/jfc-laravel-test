<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Category;
use App\Product;
use App\Order;
use App\OrderProduct;



class exampleInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //create users
      $user1 = User::create(['name'=>'Jorge', 'email'=>'negoxp@gmail.com', 'password' => bcrypt("12345678")]);
      $user2 = User::create(['name'=>'Josh', 'email'=>'josh@yaremedia.com', 'password' => bcrypt("12345678")]);

      //Create a Categories
      $electronic = Category::create(['name' => 'Electronic']);
      $furniture = Category::create(['name' => 'Furniture']);
      $automotive = Category::create(['name' => 'Automotive']);

      //Create products
      $tv = Product::create(['name' => 'LG 50" tv', 'price' => 799.99, 'category_id' => $electronic->id]);
      $sofa = Product::create(['name' => 'Love seat grey', 'price' => 400.00, 'category_id' => $furniture->id]);
      $tire = Product::create(['name' => 'Goodyear 16" tv', 'price' => 130.00, 'category_id' => $automotive->id]);

      //Create an Order
      $order1 = Order::create(['user_id' => $user1->id]);
      $order2 = Order::create(['user_id' => $user2->id]);

      //Create an Order Details
      $order1_p1 = OrderProduct::create(['order_id' => $order1->id, 'product_id'=>$tire->id, 'quantity' => 4 ]);
      $order1_p1 = OrderProduct::create(['order_id' => $order1->id, 'product_id'=>$tv->id, 'quantity' => 1 ]);
      $order2_p1 = OrderProduct::create(['order_id' => $order1->id, 'product_id'=>$sofa->id, 'quantity' => 1 ]);
      $order2_p2 = OrderProduct::create(['order_id' => $order1->id, 'product_id'=>$tv->id, 'quantity' => 1 ]);

    }
}
